import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by leven on 2017/3/2.
 */
public class OuterFunction implements Function<Tuple2<String, Iterable<Event>>, Boolean> {

    private int i;

    public OuterFunction(int i) {
        this.i = i;
    }

    @Override
    public Boolean call(Tuple2<String, Iterable<Event>> stringIterableTuple2) throws Exception {
        Collection<Event> collection = new ArrayList<>();
        stringIterableTuple2._2().forEach(collection::add);
        return collection.stream().filter(event -> event.name.equals("A") && event.owner.equals("s" + i)).count() > 0;
    }
}
