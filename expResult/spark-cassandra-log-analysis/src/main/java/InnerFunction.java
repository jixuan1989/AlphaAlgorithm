import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

/**
 * Created by leven on 2017/3/2.
 */
public class InnerFunction implements Function<Tuple2<String, Event>, Boolean> {

    private int j;

    public InnerFunction(int j) {
        this.j = j;
    }

    @Override
    public Boolean call(Tuple2<String, Event> stringEventTuple2) throws Exception {
        return stringEventTuple2._2().owner.equals("s" + j);
    }
}
