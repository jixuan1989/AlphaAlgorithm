import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hxd on 17/2/17.
 */
public class Event implements Comparable, Serializable {

    public Event(String event, Map<Pattern, String> patterns, Map<String, String> ips) {
        String[] detail = event.split("\\|");
        String ttime = detail[4].trim();
        time = ttime.equals("null") ? -1 : Integer.valueOf(ttime);
        owner = ips.get(detail[3].trim());
        name = detail[2].trim();
        for (Map.Entry<Pattern, String> entry : patterns.entrySet()) {
            Matcher matcher = entry.getKey().matcher(name);
            if (matcher.lookingAt()) {
                name = entry.getValue();
                if (matcher.groupCount() == 2)
                    another = ips.get(matcher.group(2));
                break;
            }
        }
    }

    String name;
    String owner;
    String another;
    int time;

    public int getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                ", another='" + another + '\'' +
                ", time=" + time +
                '}';
    }

    private static DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

    public String toXELFile() {
        /*
        <event>
			<string key="org:resource" value="UNDEFINED"/>
			<date key="time:timestamp" value="2008-12-09T08:22:01.527+01:00"/>
			<string key="concept:name" value="F"/>
			<string key="lifecycle:transition" value="complete"/>
		</event>
        */
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 1, 1, 10, 0, 0);
        calendar.add(Calendar.MILLISECOND, this.time);

        StringBuffer sb = new StringBuffer();
        sb.append("\t\t<event>\n");
        sb.append("\t\t\t<string key=\"org.resource\" value=\"" + owner + "\"/>\n");
        sb.append("\t\t\t<date key=\"time:timestamp\" value=\"" + format.format(calendar.getTime()) + "\"/>\n");
        sb.append("\t\t\t<string key=\"concept:name\" value=\"" + name + "_" + owner + (another == null ? "" : ("_" + another)) + "\"/>\n");
        sb.append("\t\t\t<string key=\"lifecycle:transition\" value=\"\"/>\n");
        sb.append("\t\t</event>\n");
        return sb.toString();
    }

    public String toXELFileWithReducedName() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 1, 1, 10, 0, 0);
        calendar.add(Calendar.MILLISECOND, this.time);

        StringBuffer sb = new StringBuffer();
        sb.append("\t\t<event>\n");
        sb.append("\t\t\t<string key=\"org.resource\" value=\"" + owner + "\"/>\n");
        sb.append("\t\t\t<date key=\"time:timestamp\" value=\"" + format.format(calendar.getTime()) + "\"/>\n");
        sb.append("\t\t\t<string key=\"concept:name\" value=\"" + name + "_" + owner + "\"/>\n");
        sb.append("\t\t\t<string key=\"lifecycle:transition\" value=\"\"/>\n");
        sb.append("\t\t</event>\n");
        return sb.toString();
    }


    @Override
    public int compareTo(Object o) {
        return this.time - ((Event) o).time;
    }
}
