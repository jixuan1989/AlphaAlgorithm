import scala.Tuple2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by hxd on 17/2/17.
 */
public class LocalHandler {

    public static void main(String[] args) throws IOException {
        Files.lines(Paths.get("/Users/hxd/Desktop/8n_100tps_10threads_write_event")).filter(
                line -> !(line.isEmpty() || line.equals("\n")
                        || line.startsWith(" session_id ")
                        || line.startsWith("-------------------")
                        || line.startsWith("(")
                        || line.startsWith("Consistency level"))
        ).map(line -> {
            int charat = line.indexOf("|");
            return new Tuple2(line.substring(0, charat), line.substring(charat + 1));
        });
    }

    static class Pair<T, V> {
        T _1;
        V _2;

        public Pair(T a, V b) {
            this._1 = a;
            this._2 = b;
        }

        public T _1() {
            return _1;
        }

        public V _2() {
            return _2;
        }
    }
}
