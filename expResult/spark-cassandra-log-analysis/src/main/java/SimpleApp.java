import org.apache.commons.collections.IteratorUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import scala.Tuple2;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleApp {

    public static void main(String[] args) throws IOException {
        Pattern ipPattern = Pattern.compile("\\d+\\.\\d+\\.\\d+\\.\\d+");
        String[] writeLogs = {
                /*A*/"Parsing [Ii][Nn][Ss][Ee][Rr][Tt] INTO .*",
                /*B*/"Preparing statement",
                /*C*/"Determining replicas for mutation",
                /*D*/"Sending MUTATION message to (s\\d+)?/(\\d+\\.\\d+\\.\\d+\\.\\d+)",
                /*E*/"MUTATION message received from (s\\d+)?/(\\d+\\.\\d+\\.\\d+\\.\\d+)",
                /*F*/"Appending to commitlog",
                /*G*/"Adding to usertable memtable",
                /*H*/"Enqueuing response to (s\\d+)?/(\\d+\\.\\d+\\.\\d+\\.\\d+)",
                /*I*/"Sending REQUEST_RESPONSE message to (s\\d+)?/(\\d+\\.\\d+\\.\\d+\\.\\d+)",
                /*J*/"REQUEST_RESPONSE message received from (s\\d+)?/(\\d+\\.\\d+\\.\\d+\\.\\d+)",
                /*K*/"Processing response from (s\\d+)?/(\\d+\\.\\d+\\.\\d+\\.\\d+)",
                /*L*/"Write timeout;.*"
        };
        Pattern[] eventPatterns = Arrays.stream(writeLogs).map(Pattern::compile).toArray(Pattern[]::new);
        Map<Pattern, String> eventNames = new HashMap<>();
        for (int i = 0; i < eventPatterns.length; i++) {
            eventNames.put(eventPatterns[i], "" + (char) (65 + i));
        }

        //String logFile = new Scanner(System.in).nextLine(); // Should be some file on your system
        String logFile = "4n_200tps_10threads_write_event"; // Should be some file on your system

        SparkConf conf = new SparkConf().setAppName("Simple Application");
        conf.setMaster("local[8]");
        JavaSparkContext sc = new JavaSparkContext(conf);
        Broadcast<Pattern> bipPattern = sc.broadcast(ipPattern);

        //clean log events
        JavaRDD<String> logData = sc.textFile(logFile).filter(x -> !(x.isEmpty() || x.equals("\n")
                || x.startsWith(" session_id ")
                || x.startsWith("-------------------")
                || x.startsWith("(")
                || x.startsWith("Consistency level"))).cache();

        //find out ips
        Map<String, String> ipNames = generateIPandName(logData, bipPattern);
        Broadcast<Map<String, String>> bpNames = sc.broadcast(ipNames);
        Broadcast<Map<Pattern, String>> beventNames = sc.broadcast(eventNames);


        JavaPairRDD<String, Event> pair1 = logData.mapToPair(line -> {
            int charat = line.indexOf("|");
            return new Tuple2<>(line.substring(0, charat), new Event(line.substring(charat), beventNames.getValue(), bpNames.getValue()));
        }).filter(tuple -> tuple._2().getTime() >= 0);

        for (int i = 0; i < 4; ++i) {
            OuterFunction outerFunction = new OuterFunction(i);
            JavaPairRDD<String, Iterable<Event>> pair2 = pair1.groupByKey().filter(outerFunction);
            JavaPairRDD<String, Iterable<Event>> pair3 = pair2.mapValues(iter -> {
                List<Event> events = new ArrayList<>();
                iter.forEach(events::add);
                Collections.sort(events);
                return events;
            });

            for (int j = 0; j < 4; ++j) {
                InnerFunction innerFunction = new InnerFunction(j);
                JavaPairRDD<String, String> pair4 = pair3.flatMapValues(events -> events).filter(innerFunction).mapValues(Event::toXELFile);

                JavaRDD<String> tracesRDD = pair4.reduceByKey((e1, e2) -> e1 + e2).map(SimpleApp::generateTrace);

                List<String> text = tracesRDD.collect();
                Files.write(Paths.get(String.format("4n_%d_%d_name_owner_another.result", i, j)), Files.readAllLines(Paths.get("header.event")), Charset.forName("UTF-8"));
                Files.write(Paths.get(String.format("4n_%d_%d_name_owner_another.result", i, j)), (Iterable<String>) text.stream()::iterator, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
                Files.write(Paths.get(String.format("4n_%d_%d_name_owner_another.result", i, j)), new ArrayList<String>() {{
                    add("</log>\n");
                }}, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
            }
        }
        sc.stop();
        //sc.close();

    }

    private static Map<String, String> generateIPandName(JavaRDD<String> logData, Broadcast<Pattern> bipPattern) {
        List<String> ips = logData.filter(line -> line.contains("172.30.")).flatMap(line -> {
            Matcher matcher = bipPattern.getValue().matcher(line);
            List<String> iplist = new ArrayList<>();
            while (matcher.find()) {
                iplist.add(matcher.group());
            }
            return iplist.iterator();
        }).distinct().collect();
        Map<String, String> ipNames = new HashMap<>();
        for (int i = 0; i < ips.size(); i++) {
            ipNames.put(ips.get(i), "s" + i);
        }
        return ipNames;
    }

    private static String generateTrace(Tuple2<String, String> sessionEvents) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\t<trace>\n");
        stringBuilder.append("\t\t<string key=\"concept:name\" value=\"" + sessionEvents._1() + "\"/>\n");
        stringBuilder.append(sessionEvents._2());
        stringBuilder.append("\t</trace>\n");
        return stringBuilder.toString();
    }

    private static String header() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<!-- This file has been generated with the OpenXES library. It conforms -->\n" +
                "<!-- to the XML serialization of the XES standard for log storage and -->\n" +
                "<!-- management. -->\n" +
                "<!-- XES standard version: 1.0 -->\n" +
                "<!-- OpenXES library version: 1.0RC7 -->\n" +
                "<!-- OpenXES is available from http://www.openxes.org/ -->\n" +
                "<log xes.version=\"1.0\" xes.features=\"nested-attributes\" openxes.version=\"1.0RC7\" xmlns=\"http://www.xes-standard.org/\">\n" +
                "\t<extension name=\"Lifecycle\" prefix=\"lifecycle\" uri=\"http://www.xes-standard.org/lifecycle.xesext\"/>\n" +
                "\t<extension name=\"Organizational\" prefix=\"org\" uri=\"http://www.xes-standard.org/org.xesext\"/>\n" +
                "\t<extension name=\"Time\" prefix=\"time\" uri=\"http://www.xes-standard.org/time.xesext\"/>\n" +
                "\t<extension name=\"Concept\" prefix=\"concept\" uri=\"http://www.xes-standard.org/concept.xesext\"/>\n" +
                "\t<extension name=\"Semantic\" prefix=\"semantic\" uri=\"http://www.xes-standard.org/semantic.xesext\"/>\n" +
                "\t<global scope=\"trace\">\n" +
                "\t\t<string key=\"concept:name\" value=\"__INVALID__\"/>\n" +
                "\t</global>\n" +
                "\t<global scope=\"event\">\n" +
                "\t\t<string key=\"concept:name\" value=\"__INVALID__\"/>\n" +
                "\t\t<string key=\"lifecycle:transition\" value=\"complete\"/>\n" +
                "\t</global>\n" +
                "\t<classifier name=\"MXML Legacy Classifier\" keys=\"concept:name lifecycle:transition\"/>\n" +
                "\t<classifier name=\"Event Name\" keys=\"concept:name\"/>\n" +
                "\t<classifier name=\"Resource\" keys=\"Spark\"/>\n" +
                "\t<string key=\"source\" value=\"Rapid Synthesizer\"/>\n" +
                "\t<string key=\"concept:name\" value=\"excercise3.mxml\"/>\n" +
                "\t<string key=\"lifecycle:model\" value=\"standard\"/>";
    }
}