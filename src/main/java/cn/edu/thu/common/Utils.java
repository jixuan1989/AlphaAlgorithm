package cn.edu.thu.common;

import cn.edu.thu.model.Event;
import cn.edu.thu.model.Trace;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by leven on 2017/2/22.
 */
public class Utils {

    public static Set<Trace> parseXESFile(File file/*String fileName*/) {
        //Document doc = Jsoup.parse(new File(fileName), "UTF-8");
        Document doc = null;
        try {
            doc = Jsoup.parse(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements traces = doc.getElementsByTag("trace");
        Set<Trace> eventLog = new HashSet<>();
        for (int i = 0; i < traces.size(); i++) {
            Element currentTrace = traces.get(i);
            Elements events = currentTrace.getElementsByTag("event");
            Trace newTrace = new Trace();
            for (int j = 0; j < events.size(); j++) {
                Element currentEvent = events.get(j);
                Elements name = currentEvent.getElementsByAttributeValue("key",
                        "concept:name");
                Element nameN = name.get(0);
                Elements lifec = currentEvent.getElementsByAttributeValue(
                        "key", "lifecycle:transition");
                Element lifc = lifec.get(0);
                Event ev = new Event(nameN.attr("value") + " "
                        + lifc.attr("value"));
                newTrace.add(ev);
            }
            eventLog.add(newTrace);
        }
        return eventLog;
    }
}
