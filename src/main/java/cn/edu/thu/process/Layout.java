package cn.edu.thu.process;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import cn.edu.thu.model.Event;
import cn.edu.thu.model.PresSucs;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;

public class Layout /*extends JFrame*/
{

    private static final long serialVersionUID = -578683911307318455L;

    private GraphControl graphControl;

    private Map<String, Object> transitions;
    private Map<String, Object> places;

    public Layout(String graphName, Set<PresSucs> network)
    {
        /*super(graphName);*/

        transitions = new HashMap<>();
        places = new HashMap<>();

        // Creates graph with model
        mxGraph graph = new mxGraph();
        Object parent = graph.getDefaultParent();

        mxStylesheet stylesheet = graph.getStylesheet();
        Hashtable<String, Object> style = new Hashtable<>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
        style.put(mxConstants.STYLE_OPACITY, 50);
        style.put(mxConstants.STYLE_FONTCOLOR, "#774400");
        stylesheet.putCellStyle("CIRCLE", style);

        graph.getModel().beginUpdate();
        try
        {
            for (PresSucs presSucs : network) {
                String placeName = presSucs.toString();
                Object place = graph.insertVertex(parent, null, placeName, 0, 0, 30, 30, "CIRCLE");
                places.put(placeName, place);

                Set<Event> pres = presSucs.precursors;
                for (Event precursor : pres) {
                    String transitionName = precursor.toString();
                    Object transition = transitions.get(transitionName);
                    if (transition == null) {
                        transition = graph.insertVertex(parent, null, transitionName, 0, 0, 30, 30);
                        transitions.put(transitionName, transition);
                    }
                    graph.insertEdge(parent, null, transitionName + "->" + placeName, transition, place);
                }

                Set<Event> sucs = presSucs.succeeds;
                for (Event succeed : sucs) {
                    String transitionName = succeed.toString();
                    Object transition = transitions.get(transitionName);
                    if (transition == null) {
                        transition = graph.insertVertex(parent, null, transitionName, 0, 0, 30, 30);
                        transitions.put(transitionName, transition);
                    }
                    graph.insertEdge(parent, null, placeName + "->" + transitionName, place, transition);
                }
            }

            mxIGraphLayout layout = new mxHierarchicalLayout(graph, 1);
            layout.execute(parent);
        }
        finally
        {
            graph.getModel().endUpdate();
        }

        graph.getView().setScale(1.8);

        BufferedImage image = mxCellRenderer.createBufferedImage(graph, null, 1, Color.WHITE, true, null);
        try {
            ImageIO.write(image, "PNG", new File(graphName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
        // Creates a control in a scrollpane
        graphControl = new GraphControl(graph);
        JScrollPane scrollPane = new JScrollPane(graphControl);
        scrollPane.setAutoscrolls(true);

        // Puts the control into the frame
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        setSize(new Dimension(1024, 768));*/
    }

//    public static void main(String[] args)
//    {
//        Layout frame = new Layout();
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.setVisible(true);
//    }

}
