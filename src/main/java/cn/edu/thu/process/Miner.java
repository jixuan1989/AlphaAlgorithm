package cn.edu.thu.process;

import cn.edu.thu.common.Utils;
import cn.edu.thu.model.Event;
import cn.edu.thu.model.PresSucs;
import cn.edu.thu.model.Trace;

import javax.swing.*;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.*;

public class Miner {

    private static int count = 0;

    private Collection<Trace> traces;
    private Map<Event, Set<Event>> positiveMap;
    private Map<Event, Set<Event>> reverseMap;
    private Set<Trace> parallels;

    public Miner(Collection<Trace> traces) {
        this.traces = traces;
        this.positiveMap = new HashMap<>();
        this.reverseMap = new HashMap<>();
        this.parallels = new HashSet<>();
    }

    public Set<PresSucs> mine() {
        pre_process();
        Set<PresSucs> result = process();
        return result;
    }

    private void pre_process() {
        traces.stream().filter(trace -> trace.size() > 1).forEach(trace -> {
            for (int i = 0; i < trace.size() - 1; ++i) {
                Event precursor = trace.get(i);
                Event succeed = trace.get(i + 1);

                Set<Event> succeeds = positiveMap.computeIfAbsent(precursor, obj -> new HashSet<>());
                succeeds.add(succeed);
                positiveMap.put(precursor, succeeds);

                Set<Event> precursors = reverseMap.computeIfAbsent(succeed, obj -> new HashSet<>());
                precursors.add(precursor);
                reverseMap.put(succeed, precursors);
            }
        });

        positiveMap.forEach((precursor, succeeds) ->
                succeeds.forEach(succeed -> {
                    if (positiveMap.containsKey(succeed) && positiveMap.get(succeed).contains(precursor)) {
                        parallels.add(new Trace(precursor, succeed));
                    }
                })
        );

        parallels.forEach(parallel -> {
            Event precursor = parallel.get(0);
            Event succeed = parallel.get(1);

            positiveMap.computeIfAbsent(precursor, obj -> new HashSet<>()).remove(succeed);
            if (positiveMap.get(precursor).size() == 0) positiveMap.remove(precursor);
            positiveMap.computeIfAbsent(succeed, obj -> new HashSet<>()).remove(precursor);
            if (positiveMap.get(succeed).size() == 0) positiveMap.remove(succeed);

            reverseMap.computeIfAbsent(precursor, obj -> new HashSet<>()).remove(succeed);
            if (reverseMap.get(precursor).size() == 0) reverseMap.remove(precursor);
            reverseMap.computeIfAbsent(succeed, obj -> new HashSet<>()).remove(precursor);
            if (reverseMap.get(succeed).size() == 0) reverseMap.remove(succeed);
        });
    }

    private Set<PresSucs> process() {
        Set<PresSucs> result = new HashSet<>();
        positiveMap.forEach((fellow, succeeds) -> split(result, new PresSucs(fellow, succeeds), true));

        Map<Set<Event>, Set<Event>> temp = new HashMap<>();
        result.forEach(presSucs -> {
            Set<Event> succeeds = presSucs.succeeds;
            Set<Event> precursors = presSucs.precursors;
            Set<Event> allPrecursors = temp.computeIfAbsent(succeeds, obj -> new HashSet<>());
            allPrecursors.addAll(precursors);
        });
        result.clear();

        temp.forEach((succeeds, precursors) -> split(result, new PresSucs(precursors, succeeds), false));
        return result;
    }

    private void split(Set<PresSucs> result, PresSucs presSucs, boolean isSucs) {
        Set<Event> noSplit = isSucs ? presSucs.precursors : presSucs.succeeds;
        Set<Event> toSplit = isSucs ? presSucs.succeeds : presSucs.precursors;
        if (toSplit.size() <= 1) {
            result.add(presSucs);
            return;
        }

        Event splitEventA = null;
        Event splitEventB = null;
        for (Event fellow : toSplit) {
            Trace parallel = getParallel(toSplit);
            if (null != parallel) {
                splitEventA = parallel.get(0);
                splitEventB = parallel.get(1);
                break;
            }

            splitEventB = getRelative(toSplit, positiveMap.get(fellow));
            if (null != splitEventB) {
                splitEventA = fellow;
                break;
            }

            splitEventB = getRelative(toSplit, reverseMap.get(fellow));
            if (null != splitEventB) {
                splitEventA = fellow;
                break;
            }
        }

        if (null == splitEventB) {
            result.add(presSucs);
            return;
        }

        toSplit.remove(splitEventA);
        Set<Event> subsetA = new HashSet<>(toSplit);
        PresSucs partA = isSucs ? new PresSucs(noSplit, subsetA) : new PresSucs(subsetA, noSplit);
        split(result, partA, isSucs);
        toSplit.add(splitEventA);

        toSplit.remove(splitEventB);
        Set<Event> subsetB = new HashSet<>(toSplit);
        PresSucs partB = isSucs ? new PresSucs(noSplit, subsetB) : new PresSucs(subsetB, noSplit);
        split(result, partB, isSucs);
    }

    private Trace getParallel(Set<Event> fellows) {
        for (Event fellow : fellows) {
            for (Trace trace : parallels) {
                if (trace.get(0).equals(fellow) && fellows.contains(trace.get(1)))
                    return trace;

                if (trace.get(1).equals(fellow) && fellows.contains(trace.get(0)))
                    return trace;
            }
        }

        return null;
    }

    private Event getRelative(Set<Event> fellows, Set<Event> relatives) {
        if (null == relatives)
            return null;

        for (Event relative : relatives) {
            if (fellows.contains(relative)) {
                return relative;
            }
        }

        return null;
    }


    public static void main(String[] args) throws IOException {

        File dir = new File("ExpData");
        Arrays.stream(dir.listFiles((file, name) -> name.contains(".result"))).forEach(
                file -> {
                    long start = System.currentTimeMillis();
                    Set<Trace> traces = Utils.parseXESFile(file);
                    Miner miner = new Miner(traces);
                    Set<PresSucs> result = miner.mine();
                    long end = System.currentTimeMillis();
                    System.out.println("\n\nalpha algorithm : " + (end - start));

                    System.out.println("place number : " + result.size());
                    Set<String> transitions = new HashSet<>();
                    for (PresSucs presSucs : result) {
                        presSucs.precursors.forEach(precursor -> transitions.add(precursor.getName()));
                        presSucs.succeeds.forEach(succeed -> transitions.add(succeed.getName()));
                    }
                    System.out.println("transition number : " + transitions.size());

                    start = System.currentTimeMillis();
                    Layout frame1 = new Layout(file.getName(), result);
                    /*frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame1.setVisible(true);*/
                    end = System.currentTimeMillis();
                    System.out.println("draw picture : " + (end - start));
                }
        );
//
//        start = System.currentTimeMillis();
//        Layout frame1 = new Layout(result, 10);
//        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame1.setVisible(true);
//        end = System.currentTimeMillis();
//        System.out.println("draw picture : " + (end - start));
//
//        start = System.currentTimeMillis();
//        Layout frame2 = new Layout(result, 20);
//        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame2.setVisible(true);
//        end = System.currentTimeMillis();
//        System.out.println("draw picture : " + (end - start));
//
//        start = System.currentTimeMillis();
//        Layout frame3 = new Layout(result, 40);
//        frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame3.setVisible(true);
//        end = System.currentTimeMillis();
//        System.out.println("draw picture : " + (end - start));
//
//        start = System.currentTimeMillis();
//        Layout frame4 = new Layout(result, 80);
//        frame4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame4.setVisible(true);
//        end = System.currentTimeMillis();
//        System.out.println("draw picture : " + (end - start));
//
//        start = System.currentTimeMillis();
//        Layout frame5 = new Layout(result, 160);
//        frame5.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame5.setVisible(true);
//        end = System.currentTimeMillis();
//        System.out.println("draw picture : " + (end - start));
//
//        start = System.currentTimeMillis();
//        Layout frame6 = new Layout(result, 320);
//        frame6.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame6.setVisible(true);
//        end = System.currentTimeMillis();
//        System.out.println("draw picture : " + (end - start));
    }

    /***********************************************************8
     //        start = System.currentTimeMillis();
     //        Graph graph = new SingleGraph("workflow");
     //        graph.addAttribute("ui.stylesheet", styleSheet);
     //        graph.setAutoCreate(true);
     //        graph.setStrict(false);
     //        graph.display();
     //
     //        Set<String> places = new HashSet<>();
     //        for (PresSucs presSucs : result) {
     //            String place = presSucs.toString();
     //            places.add(place);
     //
     //            Set<Event> pres = presSucs.precursors;
     //            for (Event precursor : pres) {
     //                graph.addEdge(precursor.toString() + place, precursor.toString(), place);
     //            }
     //
     //            Set<Event> sucs = presSucs.succeeds;
     //            for (Event succeed : sucs) {
     //                graph.addEdge(place + succeed.toString(), place, succeed.toString());
     //            }
     //        }
     //        end = System.currentTimeMillis();
     //        System.out.println("draw picture : " + (end - start));
     //
     ////        for (Node node : graph) {
     ////            node.addAttribute("ui.label", node.getId());
     ////            explore(node, places);
     ////        }
     //    }
     //
     //    protected static String styleSheet =
     //            "node { fill-color: black;}" +
     //                    "node.marked {fill-color: red;}";
     //
     //    public static void explore(Node source, Set<String> places) {
     //        Iterator<? extends Node> k = source.getBreadthFirstIterator();
     //        while (k.hasNext()) {
     //            Node next = k.next();
     //            if (places.contains(next.toString())) {
     //                next.setAttribute("ui.class", "marked");
     //            }
     //        }
     //    }*/
}
