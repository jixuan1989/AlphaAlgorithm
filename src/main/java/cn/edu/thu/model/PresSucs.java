package cn.edu.thu.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by leven on 2017/2/21.
 */
public class PresSucs {

    public Set<Event> precursors;
    public Set<Event> succeeds;

    public PresSucs() {
        this.precursors = new HashSet<>();
        this.precursors = new HashSet<>();
    }

    public PresSucs(Set<Event> precursors, Set<Event> succeeds) {
        this.precursors = precursors;
        this.succeeds = succeeds;
    }

    public PresSucs(Event fellow, Set<Event> succeeds) {
        this.precursors = new HashSet<>();
        precursors.add(fellow);

        this.succeeds = succeeds;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        precursors.forEach(stringBuilder::append);
        stringBuilder.append("->");
        succeeds.forEach(stringBuilder::append);
        return stringBuilder.toString();
    }
}
