package cn.edu.thu.model;

/**
 * Created by leven on 2017/2/22.
 */
public class Event {

    private String name;

    public Event(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof Event))
            return false;

        Event event = (Event) obj;
        return this.name.equals(event.name);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public String toString() {
        return this.name;
    }
}
