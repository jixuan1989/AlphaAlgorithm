package cn.edu.thu.model;

import java.util.ArrayList;

/**
 * Created by leven on 2017/2/22.
 */
public class Trace extends ArrayList<Event> {

    public Trace() {
        super();
    }

    public Trace(String... events) {
        super();
        for (String event : events) {
            this.add(new Event(event));
        }
    }

    public Trace(Event... events) {
        super();
        for (Event event : events) {
            this.add(event);
        }
    }
}
