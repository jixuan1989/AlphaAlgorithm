package cn.edu.thu.old;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by leven on 2017/2/21.
 */
public class Pair {

    public Set<Character> left;
    public Set<Character> right;

    public Pair() {
        this.left = new HashSet<>();
        this.right = new HashSet<>();
    }

    public Pair(Character a, Character b) {
        this.left = new HashSet<>();
        this.right = new HashSet<>();

        this.left.add(a);
        this.right.add(b);
    }

    public Pair(Set<Character> left, Set<Character> right) {
        this.left = left;
        this.right = right;
    }

    public Pair(Character a, Set<Character> right) {
        this.left = new HashSet<>();
        this.left.add(a);

        this.right = right;
    }
}
