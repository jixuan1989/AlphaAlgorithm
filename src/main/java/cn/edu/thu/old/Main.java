package cn.edu.thu.old;

import java.util.*;

public class Main {

    private static Set<String> links = new TreeSet<>();
    private static Set<String> parallels = new TreeSet<>();

    public static void pre_process(List<String> traces) {
        traces.forEach(Main::getLink);
        links.removeAll(parallels);
    }

    private static void getLink(String trace) {
        int len = trace.length();
        for (int i = 0; i < len - 1; ++i) {
            String link = trace.substring(i, i + 2);
            links.add(link);

            String counter = new StringBuilder(link).reverse().toString();
            if (links.contains(counter)) {
                parallels.add(link);
                parallels.add(counter);
            }
        }
    }

    public static void process() {
        Set<Pair> result = new HashSet<>();

        Pair rightSet = null;
        Character lastLeft = ' ';
        for (String link : links) {
            Character a = link.charAt(0);
            Character b = link.charAt(1);

            if (lastLeft != a) {
                if (null != rightSet) {
                    split(result, rightSet, true);
                }
                rightSet = new Pair(a, b);
                lastLeft = a;
            }
            else {
                rightSet.right.add(b);
            }
        }

        if (null != rightSet) {
            split(result, rightSet, true);
        }

        List<Pair> result1 = new ArrayList<>(result);
        Collections.sort(result1, (a, b) -> compare(a.right, b.right));
        result.clear();

        Pair leftSet = null;
        Set<Character> lastRight = new HashSet<>();
        for (Pair pair : result1) {
            Set<Character> left = pair.left;
            Set<Character> right = pair.right;

            if (!equals(lastRight, right)) {
                if (null != leftSet) {
                    split(result, leftSet, false);
                }
                leftSet = pair;
                lastRight = right;
            }
            else {
                leftSet.left.addAll(left);
            }
        }

        if (null != leftSet) {
            split(result, leftSet, false);
        }

        System.out.println();
    }

    private static void split(Set<Pair> result, Pair pair, boolean isRight) {
        Set<Character> noSplit = isRight ? pair.left : pair.right;
        Set<Character> toSplit = isRight ? pair.right : pair.left;

        String splitLink = null;
        for (Character aChar : toSplit) {
            for (String link : links) {
                Character relative = contains(link, aChar);
                if (null != relative && toSplit.contains(relative)) {
                    splitLink = link;
                    break;
                }
            }

            if (null != splitLink) {
                break;
            }

            for (String link : parallels) {
                Character relative = contains(link, aChar);
                if (null != relative && toSplit.contains(relative)) {
                    splitLink = link;
                    break;
                }
            }
        }

        if (null == splitLink) {
            result.add(pair);
            return;
        }

        Character leftSplitPoint = splitLink.charAt(0);
        Character rightSplitPoint = splitLink.charAt(1);
        toSplit.remove(leftSplitPoint);
        Set<Character> subLeft = new HashSet<>(toSplit);
        Pair sub1 = isRight ? new Pair(noSplit, subLeft) : new Pair(subLeft, noSplit);
        split(result, sub1, isRight);
        toSplit.add(leftSplitPoint);

        toSplit.remove(rightSplitPoint);
        Set<Character> subRight = new HashSet<>(toSplit);
        Pair sub2 = isRight ? new Pair(noSplit, subRight) : new Pair(subRight, noSplit);
        split(result, sub2, isRight);
    }

    private static Character contains(String link, Character aChar) {
        if (link.charAt(0) == aChar) {
            return link.charAt(1);
        }
        if (link.charAt(1) == aChar) {
            return link.charAt(0);
        }
        return null;
    }

    private static int compare(Set<Character> a, Set<Character> b) {
        List<Character> aList = new ArrayList<>(a);
        List<Character> bList = new ArrayList<>(b);

        Collections.sort(aList);
        Collections.sort(bList);

        for (int i = 0; i < aList.size() && i < bList.size(); ++i) {
            if (aList.get(i) < bList.get(i)) return -1;
            if (aList.get(i) > bList.get(i)) return 1;
        }

        return 0;
    }

    private static boolean equals(Set<Character> a, Set<Character> b) {
        return subSet(a, b) && subSet(b, a);
    }

    private static boolean subSet(Set<Character> a, Set<Character> b) {
        for (Character aChar : a) {
            if (!b.contains(aChar)) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        List<String> traces = new ArrayList<>();
        traces.add("abcd");
        traces.add("acbd");
        traces.add("aed");

        pre_process(traces);
        links.forEach(System.out::println);
        System.out.println();
        parallels.forEach(System.out::println);
        System.out.println();

        process();
    }

}
